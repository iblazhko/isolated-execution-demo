﻿namespace IsolatedExecution
{
    using System;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// Wrapper for an executor instance.
    /// </summary>
    public class IsolatedExecutor : IIsolatedExecutor
    {
        /// <summary>
        /// Indicates whether this object instance is disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedExecutor"/> class.
        /// </summary>
        /// <param name="instance">The instance to wrap.</param>
        /// <param name="sandboxId">Id of the sandbox this instance belongs to.</param>
        public IsolatedExecutor(object instance, Guid sandboxId)
            : this(instance, sandboxId, Guid.NewGuid())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedExecutor"/> class.
        /// </summary>
        /// <param name="instance">The instance to wrap.</param>
        /// <param name="sandboxId">Id of the sandbox this instance belongs to.</param>
        /// <param name="instanceId">Wrapper instanceId.</param>
        public IsolatedExecutor(object instance, Guid sandboxId, Guid instanceId)
        {
            Contract.Requires(instance != null);
            Contract.Requires(instanceId != Guid.Empty);
            Contract.Requires(sandboxId != Guid.Empty);

            this.Instance = instance;
            this.InstanceId = instanceId;
            this.SandboxId = sandboxId;
        }

        /// <inheritdoc />
        public object Instance { get; private set; }

        /// <inheritdoc />
        public Guid InstanceId { get; private set; }

        /// <inheritdoc />
        public Guid SandboxId { get; private set; }

        /// <inheritdoc />
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose wrapped instance.
        /// </summary>
        /// <param name="disposing">Indicates whether the call is coming from
        /// Dispose (as opposed to Finalize).</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.disposed)
                {
                    return;
                }

                if (this.Instance != null)
                {
                    var disposableInstance = this.Instance as IDisposable;
                    this.Instance = null;

                    if (disposableInstance != null)
                    {
                        disposableInstance.Dispose();
                    }
                }

                this.InstanceId = Guid.Empty;

                this.disposed = true;
            }
        }
    }
}
