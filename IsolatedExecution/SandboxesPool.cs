﻿namespace IsolatedExecution
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Linq;

    /// <summary>
    /// Pool of sandboxes for isolated executor instances.
    /// </summary>
    public class SandboxesPool : ISandboxesPool
    {
        /// <summary>
        /// Default isolation mode value used by CreateIsolatedInstance.
        /// </summary>
        private readonly ExecutorIsolationMode defaultIsolationMode;

        /// <summary>
        /// Sandboxes collection.
        /// </summary>
        private IList<ISandbox> sandboxesList;

        /// <summary>
        /// Sandboxes collection synchronization object.
        /// </summary>
        private object sandboxesSync;

        /// <summary>
        /// Indicates whether the sandbox has been disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxesPool" /> class.
        /// </summary>
        public SandboxesPool()
            : this(ExecutorIsolationMode.FreeSandbox)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxesPool" /> class.
        /// </summary>
        /// <param name="defaultIsolationMode">Default isolation mode value.</param>
        public SandboxesPool(ExecutorIsolationMode defaultIsolationMode)
        {
            this.defaultIsolationMode = defaultIsolationMode;
            this.sandboxesList = new List<ISandbox>();
            this.sandboxesSync = new object();
        }

        /// <inheritdoc />
        public IEnumerable<ISandbox> Sandboxes
        {
            get
            {
                ISandbox[] sandboxes;
                lock (this.sandboxesSync)
                {
                    sandboxes = this.sandboxesList != null ? this.sandboxesList.ToArray() : new ISandbox[] { };
                }

                return sandboxes;
            }
        }

        /// <inheritdoc />
        public Guid CreateExecutorSandbox()
        {
            var sandbox = this.CreateNewSandbox(SandboxSharingMode.Shared);
            return sandbox.Id;
        }

        /// <inheritdoc />
        public void DisposeExecutorSandbox(Guid sandboxId)
        {
            Contract.Requires(sandboxId != Guid.Empty);

            lock (this.sandboxesSync)
            {
                var sandbox = this.sandboxesList.First(s => s.Id == sandboxId);
                this.RemoveSandbox(sandbox);
            }
        }

        /// <inheritdoc />
        public IIsolatedExecutor CreateIsolatedInstance(Type executorType)
        {
            Contract.Requires(executorType != null);

            return this.CreateIsolatedInstance(executorType, this.defaultIsolationMode);
        }

        /// <inheritdoc />
        public IIsolatedExecutor CreateIsolatedInstance(Type executorType, ExecutorIsolationMode mode)
        {
            Contract.Requires(executorType != null);

            var sandbox = this.GetSandboxByIsolationMode(mode);
            return this.CreateIsolatedInstance(sandbox, executorType);
        }

        /// <inheritdoc />
        public IIsolatedExecutor CreateIsolatedInstance(Type executorType, Guid sandboxId)
        {
            Contract.Requires(executorType != null);
            Contract.Requires(sandboxId != Guid.Empty);

            lock (this.sandboxesSync)
            {
                var sandbox = this.sandboxesList.First(s => s.Id == sandboxId);
                return this.CreateIsolatedInstance(sandbox, executorType);
            }
        }

        /// <inheritdoc />
        public void DisposeIsolatedInstance(Guid instanceId)
        {
            Contract.Requires(instanceId != Guid.Empty);

            lock (this.sandboxesSync)
            {
                this.DisposeIsolatedInstance(instanceId, SandboxAutoDisposalMode.None);
            }
        }

        /// <inheritdoc />
        public void DisposeIsolatedInstance(Guid instanceId, SandboxAutoDisposalMode autoDisposalMode)
        {
            Contract.Requires(instanceId != Guid.Empty);

            var sandbox = this.FindSandboxByExecutor(instanceId);
            if (sandbox == null)
            {
                throw new SandboxesPoolException("Sandboxes pool does not contain isolated instance with id=" + instanceId);
            }

            sandbox.DisposeIsolatedInstance(instanceId);

            if (this.IsSandboxSuitableForAutoDispose(sandbox))
            {
                switch (autoDisposalMode)
                {
                    case SandboxAutoDisposalMode.None:
                        break;

                    case SandboxAutoDisposalMode.DisposeIfFree:
                        this.RemoveSandbox(sandbox);
                        break;

                    default:
                        throw new ApplicationException(string.Format("SandboxAutoDisposalMode {0} is not supported", autoDisposalMode));
                }
            }
        }

        /// <inheritdoc />
        public void DisposeUnusedSandboxes()
        {
            lock (this.sandboxesSync)
            {
                var unusedSansboxes = this.sandboxesList.Where(s => s.Executors.Count() == 0).ToArray();
                foreach (var sandbox in unusedSansboxes)
                {
                    this.RemoveSandbox(sandbox);
                }
            }
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose wrapped instance.
        /// </summary>
        /// <param name="disposing">Whether the call is coming from
        /// Dispose (as opposed to Finalize)</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.disposed)
                {
                    return;
                }

                lock (this.sandboxesSync)
                {
                    var allSandboxes = this.sandboxesList.ToArray();
                    foreach (var sandbox in allSandboxes)
                    {
                        this.RemoveSandbox(sandbox);
                    }
                }

                this.sandboxesList = null;

                this.disposed = true;
            }
        }

        /// <summary>
        /// Get execution sandbox by isolation mode.
        /// </summary>
        /// <param name="mode">Isolation mode.</param>
        /// <returns>Execution isolation sandbox.</returns>
        private ISandbox GetSandboxByIsolationMode(ExecutorIsolationMode mode)
        {
            ISandbox sandbox;
            switch (mode)
            {
                case ExecutorIsolationMode.NewSandbox:
                    lock (this.sandboxesSync)
                    {
                        sandbox = this.CreateNewSandbox(SandboxSharingMode.Exclusive);
                    }

                    break;

                case ExecutorIsolationMode.FreeSandbox:
                    lock (this.sandboxesSync)
                    {
                        sandbox = this.sandboxesList.FirstOrDefault(s => 
                            (s.SharingMode == SandboxSharingMode.Shared) &&
                            (s.Executors.Count() == 0));
                        if (sandbox == null)
                        {
                            sandbox = this.CreateNewSandbox(SandboxSharingMode.Shared);
                        }
                    }

                    break;

                case ExecutorIsolationMode.LeastUtilizedSandbox:
                    lock (this.sandboxesSync)
                    {
                        sandbox = this.sandboxesList
                            .Where(s => s.SharingMode == SandboxSharingMode.Shared)
                            .OrderBy(s => s.Executors.Count())
                            .FirstOrDefault();
                        if (sandbox == null)
                        {
                            sandbox = this.CreateNewSandbox(SandboxSharingMode.Shared);
                        }
                    }

                    break;

                default:
                    throw new SandboxesPoolException(string.Format("Instance isolation mode {0} is not supported", mode));
            }

            return sandbox;
        }

        /// <summary>
        /// Create a new isolation sandbox and add it to the internal Sandboxes collection.
        /// </summary>
        /// <param name="sharingMode">Sandbox sharing mode.</param>
        /// <returns>Isolation sandbox.</returns>
        private ISandbox CreateNewSandbox(SandboxSharingMode sharingMode)
        {
            ISandbox sandbox = new Sandbox(sharingMode);
            this.sandboxesList.Add(sandbox);

            return sandbox;
        }

        /// <summary>
        /// Remove given sandbox from sandboxes collection and dispose the instance.
        /// </summary>
        /// <param name="sandbox">Sandbox instance to remove.</param>
        private void RemoveSandbox(ISandbox sandbox)
        {
            Contract.Requires(sandbox != null);

            if (!this.sandboxesList.Contains(sandbox))
            {
                throw new SandboxesPoolException("Sandboxes pool does not contain sandbox with id=" + sandbox.Id);
            }

            this.sandboxesList.Remove(sandbox);
            sandbox.Dispose();
        }

        /// <summary>
        /// Create isolated executor instance.
        /// </summary>
        /// <param name="sandbox">Sandbox instance to put the new executor instance into.</param>
        /// <param name="executorType">Executor type.</param>
        /// <returns>Isolated executor instance.</returns>
        private IIsolatedExecutor CreateIsolatedInstance(ISandbox sandbox, Type executorType)
        {
            Contract.Requires(sandbox != null);
            Contract.Requires(executorType != null);

            return sandbox.CreateIsolatedInstance(executorType);
        }

        /// <summary>
        /// Find sandbox instance that contains executor with given Id.
        /// </summary>
        /// <param name="instanceId">Executor instance id.</param>
        /// <returns>Sandbox instance.</returns>
        private ISandbox FindSandboxByExecutor(Guid instanceId)
        {
            foreach (var sandbox in this.sandboxesList)
            {
                if (sandbox.Executors.Count(e => e.InstanceId == instanceId) == 1)
                {
                    return sandbox;
                }
            }

            return null;
        }

        /// <summary>
        /// Check if the sandbox is allowed to be disposed automatically
        /// when it has no executor instances.
        /// </summary>
        /// <param name="sandbox">Sandbox instance</param>
        /// <returns>Sandbox is allowed to be disposed automatically.</returns>
        private bool IsSandboxSuitableForAutoDispose(ISandbox sandbox)
        {
            return (sandbox.SharingMode == SandboxSharingMode.Shared) &&
                (sandbox.Executors.Count() == 0);
        }
    }
}
