﻿namespace IsolatedExecution
{
    /// <summary>
    /// The enumeration defines isolation mode, that is whether
    /// a new sandbox should be created when isolating an executor,
    /// or an existing sandbox from the pool can be reused to
    /// share the executors.
    /// </summary>
    public enum ExecutorIsolationMode
    {
        /// <summary>
        /// Create a new dedicated sandbox when creating
        /// a new isolated executor instance.
        /// </summary>
        NewSandbox,

        /// <summary>
        /// Pick a free sandbox from a pool when creating
        /// a new isolated executor instance.
        /// </summary>
        /// <remarks>
        /// <para>New sandbox will be created if there are no
        /// free sandboxes available.</para>
        /// <para>If there are several free sandboxes,
        /// one will be selected randomly.</para>
        /// </remarks>
        FreeSandbox,

        /// <summary>
        /// Pick a least utilized sandbox from a pool when creating
        /// a new isolated executor instance.
        /// </summary>
        /// <remarks>
        /// <para>New sandbox will be created if there are no
        /// free sandboxes available.</para>
        /// <para>If there are several sandboxes with the same utilization,
        /// one will be selected randomly.</para>
        /// </remarks>
        LeastUtilizedSandbox
    }
}
