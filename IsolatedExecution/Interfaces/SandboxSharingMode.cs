﻿namespace IsolatedExecution
{
    /// <summary>
    /// The enumeration defines sandbox sharingmode, that is whether
    /// a sandbox can be used to contain several executors or only
    /// a one dedicated executor.
    /// </summary>
    public enum SandboxSharingMode
    {
        /// <summary>
        /// A sandbox is used to contain exactly one
        /// isolated executor instance.
        /// </summary>
        Exclusive,

        /// <summary>
        /// A sandbox is used to contain zero or more
        /// shared executor instances that are isolated
        /// as a group.
        /// </summary>
        Shared
    }
}
