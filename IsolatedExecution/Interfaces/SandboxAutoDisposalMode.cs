﻿namespace IsolatedExecution
{
    /// <summary>
    /// The enumeration defines whether a sandbox should be disposed automatically
    /// when last executor in the sandbox is disposed and the sandbox becomes free.
    /// </summary>
    /// <remarks>Applicable only for sandboxes with sharing mode
    /// <see cref="SandboxSharingMode.Shared"/></remarks>
    public enum SandboxAutoDisposalMode
    {
        /// <summary>
        /// Leave the sandbox in the pool so it is available
        /// for the next CreateIsolatedInstance call.
        /// </summary>
        None,

        /// <summary>
        /// Dispose the sandbox if it has no isolated executors.
        /// </summary>
        DisposeIfFree
    }
}
