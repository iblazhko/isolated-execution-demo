﻿namespace IsolatedExecution
{
    using System;

    /// <summary>
    /// Wrapper for an executor instance.
    /// </summary>
    public interface IIsolatedExecutor : IDisposable
    {
        /// <summary>
        /// Gets the wrapped instance Id.
        /// </summary>
        Guid InstanceId { get; }

        /// <summary>
        /// Gets Id of sandbox this instance belongs to.
        /// </summary>
        Guid SandboxId { get; }

        /// <summary>
        /// Gets the actual executor instance.
        /// </summary>
        object Instance { get; }
    }
}
