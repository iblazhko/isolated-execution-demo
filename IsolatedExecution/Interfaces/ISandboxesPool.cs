﻿namespace IsolatedExecution
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Pool of sandboxes for isolated executor instances.
    /// </summary>
    public interface ISandboxesPool : IDisposable
    {
        /// <summary>
        /// Gets list of allocated sandboxes.
        /// </summary>
        IEnumerable<ISandbox> Sandboxes { get; }

        /// <summary>
        /// Create a sandbox that can contain isolated executors.
        /// </summary>
        /// <returns>Sandbox Id.</returns>
        /// <remarks>The sandbox is created with
        /// sharing mode <see cref="SandboxSharingMode.Shared"/>.</remarks>
        Guid CreateExecutorSandbox();

        /// <summary>
        /// Dispose sandbox with the given Id.
        /// </summary>
        /// <param name="sandboxId">Sandbox identifier.</param>
        /// <remarks>If the sandbox has executors,
        /// these executors will be disposed as well.</remarks>
        void DisposeExecutorSandbox(Guid sandboxId);

        /// <summary>
        /// Create an isolated executor instance and put it in a sandbox.
        /// </summary>
        /// <param name="executorType">Type of the executor.</param>
        /// <returns>Isolated executor wrapper.</returns>
        /// <remarks>Default ExecutorIsolationMode will be used.</remarks>
        IIsolatedExecutor CreateIsolatedInstance(Type executorType);

        /// <summary>
        /// Create an isolated executor instance and put it in a sandbox.
        /// </summary>
        /// <param name="executorType">Type of the executor.</param>
        /// <param name="mode">Defines how to allocate the sandbox for the executor.</param>
        /// <returns>Isolated executor wrapper.</returns>
        IIsolatedExecutor CreateIsolatedInstance(Type executorType, ExecutorIsolationMode mode);

        /// <summary>
        /// Create an isolated executor instance in the specified sandbox.
        /// </summary>
        /// <param name="executorType">Type of the executor.</param>
        /// <param name="sandboxId">Id of the sandbox for the executor.</param>
        /// <returns>Isolated executor wrapper.</returns>
        /// <remarks>The specified sandbox is expecyed to have 
        /// sharing mode <see cref="SandboxSharingMode.Shared"/>.
        /// </remarks>
        IIsolatedExecutor CreateIsolatedInstance(Type executorType, Guid sandboxId);

        /// <summary>
        /// Dispose isolated executor.
        /// </summary>
        /// <param name="instanceId">Isolated executor Id.</param>
        void DisposeIsolatedInstance(Guid instanceId);

        /// <summary>
        /// Dispose isolated executor.
        /// </summary>
        /// <param name="instanceId">Isolated executor Id.</param>
        /// <param name="autoDisposalMode">Defines whether to dispose corresponding
        /// shared sandbox if the sandbox becomes free.</param>
        void DisposeIsolatedInstance(Guid instanceId, SandboxAutoDisposalMode autoDisposalMode);
        
        /// <summary>
        /// Dispose all unused sandboxes.
        /// </summary>
        void DisposeUnusedSandboxes();
    }
}
