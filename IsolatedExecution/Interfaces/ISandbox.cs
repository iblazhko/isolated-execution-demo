﻿namespace IsolatedExecution
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Sandbox for one or many isolated executor instances.
    /// </summary>
    /// <remarks>Sandbox instances are not supposed to be created directly.
    /// Use <see cref="ISandboxesPool"/> instead.</remarks>
    public interface ISandbox : IDisposable
    {
        /// <summary>
        /// Gets the sandbox Id.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets the sandbox sharing mode.
        /// </summary>
        SandboxSharingMode SharingMode { get; }

        /// <summary>
        /// Gets the list of executor in this sandbox.
        /// </summary>        
        IEnumerable<IIsolatedExecutor> Executors { get; }

        /// <summary>
        /// Create an isolated executor instance in this sandbox.
        /// </summary>
        /// <param name="executorType">Type of the executor.</param>
        /// <returns>Isolated executor wrapper.</returns>
        IIsolatedExecutor CreateIsolatedInstance(Type executorType);

        /// <summary>
        /// Dispose isolated wrapper with the given Id and remove corresponding item from
        /// Executors collection.
        /// </summary>
        /// <param name="instanceId">Instance instanceId.</param>
        void DisposeIsolatedInstance(Guid instanceId);

        /// <summary>
        /// Dispose given isolated wrapper and remove corresponding item from
        /// Executors collection.
        /// </summary>
        /// <param name="instance">Isolated executor wrapper.</param>
        void DisposeIsolatedInstance(IIsolatedExecutor instance);
    }
}
