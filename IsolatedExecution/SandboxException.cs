﻿namespace IsolatedExecution
{
    using System;

    /// <summary>
    /// Represents sandbox-related exceptions.
    /// </summary>
    public class SandboxException: Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxException"/> class.
        /// </summary>
        public SandboxException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public SandboxException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">Underlying exception instance.</param>
        public SandboxException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
