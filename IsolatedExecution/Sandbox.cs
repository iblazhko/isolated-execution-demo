﻿namespace IsolatedExecution
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security;
    using System.Security.Permissions;
    using System.Security.Policy;

    /// <inheritdoc />
    public class Sandbox : ISandbox
    {
        /// <summary>
        /// Indicates whether this object is disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Collection of executors.
        /// </summary>
        private IList<IIsolatedExecutor> executorsList;

        /// <summary>
        /// Executors collection synchronization object.
        /// </summary>
        private object executorsSync;

        /// <summary>
        /// Initializes a new instance of the ExecutorSandbox class./>
        /// </summary>
        public Sandbox()
            : this(
                Guid.NewGuid(),
                SandboxSharingMode.Shared,
                CreateIsolationDomain,
                GetIsolationDomainSetup)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ExecutorSandbox class./>
        /// </summary>
        /// <param name="sharingMode">Sandbox sharing mode.</param>
        public Sandbox(SandboxSharingMode sharingMode)
            : this(
                Guid.NewGuid(),
                sharingMode,
                CreateIsolationDomain,
                GetIsolationDomainSetup)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ExecutorSandbox class./>
        /// </summary>
        /// <param name="id">Sandbox instanceId.</param>
        /// <param name="isolationDomainFactory">Isolation AppDomain creation factory.</param>
        /// <param name="isolationDomainSetupFactory">Isolation AppDomain setup information factory.</param>
        public Sandbox(
            Guid id,
            Func<string, Func<Tuple<string, string, Evidence>>, AppDomain> isolationDomainFactory,
            Func<Tuple<string, string, Evidence>> isolationDomainSetupFactory)
            : this(id, SandboxSharingMode.Shared, isolationDomainFactory, isolationDomainSetupFactory)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ExecutorSandbox class./>
        /// </summary>
        /// <param name="id">Sandbox instanceId.</param>
        /// <param name="sharingMode">Sandbox sharing mode.</param>
        /// <param name="isolationDomainFactory">Isolation AppDomain creation factory.</param>
        /// <param name="isolationDomainSetupFactory">Isolation AppDomain setup information factory.</param>
        public Sandbox(
            Guid id,
            SandboxSharingMode sharingMode,
            Func<string, Func<Tuple<string, string, Evidence>>, AppDomain> isolationDomainFactory,
            Func<Tuple<string, string, Evidence>> isolationDomainSetupFactory)
        {
            Contract.Requires(id != Guid.Empty);
            Contract.Requires(isolationDomainFactory != null);
            Contract.Requires(isolationDomainSetupFactory != null);

            this.Id = id;
            this.SharingMode = sharingMode;
            this.executorsList = new List<IIsolatedExecutor>();
            this.executorsSync = new object();
            this.IsolationDomain = isolationDomainFactory(this.IsolationDomainName, isolationDomainSetupFactory);
        }

        /// <inheritdoc />
        public Guid Id { get; private set; }

        /// <inheritdoc />
        public SandboxSharingMode SharingMode { get; private set; }

        /// <inheritdoc />
        public IEnumerable<IIsolatedExecutor> Executors
        {
            get
            {
                IIsolatedExecutor[] executors;

                lock (this.executorsSync)
                {
                    executors = this.executorsList != null ? this.executorsList.ToArray() : new IIsolatedExecutor[] { };
                }

                return executors;
            }
        }

        /// <summary>
        /// Gets the application domain implementing the executor isolation.
        /// </summary>
        public AppDomain IsolationDomain { get; private set; }

        /// <summary>
        /// Gets isolation domain friendly name.
        /// </summary>
        private string IsolationDomainName
        {
            get { return string.Format(@"ExecutionSandbox_{0}", this.Id.ToString()); }
        }

        /// <summary>
        /// Create application domain for executors isolation.
        /// </summary>
        /// <param name="name">AppDomain friendly name.</param>
        /// <param name="appDomainSetupFactory">Isolation application domain setup information factory.</param>
        /// <returns>AppDomain instance.</returns>
        public static AppDomain CreateIsolationDomain(string name, Func<Tuple<string, string, Evidence>> appDomainSetupFactory)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));
            Contract.Requires(appDomainSetupFactory != null);

            var appDomainSetup = appDomainSetupFactory();

            var appBasePath = appDomainSetup.Item1;
            var appPrivateBinPath = appDomainSetup.Item2;
            var appEvidence = appDomainSetup.Item3;

            var setup = new AppDomainSetup
            {
                ApplicationName = name,
                ApplicationBase = appBasePath,
                PrivateBinPath = appPrivateBinPath,

                ApplicationTrust = new ApplicationTrust
                {
                    // Read this blog article about trust is changing in CLR4
                    // http://blogs.msdn.com/shawnfa/archive/2009/06/08/more-implicit-uses-of-cas-policy-loadfromremotesources.aspx
                    IsApplicationTrustedToRun = true,
                    DefaultGrantSet = new PolicyStatement(new PermissionSet(PermissionState.Unrestricted))
                },

                LoaderOptimization = LoaderOptimization.MultiDomain
            };

            return AppDomain.CreateDomain(name, appEvidence, setup);
        }

        /// <summary>
        /// Get application domain setup information.
        /// </summary>
        /// <returns>Base path, private bin path, and application evidence.</returns>
        public static Tuple<string, string, Evidence> GetIsolationDomainSetup()
        {
            var appBasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var appPrivateBinPath = appBasePath;
            var appEvidence = AppDomain.CurrentDomain.Evidence;

            return new Tuple<string, string, Evidence>(appBasePath, appPrivateBinPath, appEvidence);
        }

        /// <inheritdoc />
        public IIsolatedExecutor CreateIsolatedInstance(Type executorType)
        {
            if (this.SharingMode == SandboxSharingMode.Exclusive)
            {
                lock (this.executorsSync)
                {
                    if (this.executorsList.Count == 1)
                    {
                        throw new SandboxException("Dedicated sandbox cannot contain multiple isolated instances");
                    }
                }
            }

            var instance = this.IsolationDomain.CreateInstanceAndUnwrap(executorType.Assembly.FullName, executorType.FullName);
            var wrapper = new IsolatedExecutor(instance, this.Id);
            lock (this.executorsSync)
            {
                this.executorsList.Add(wrapper);
            }

            return wrapper;
        }

        /// <inheritdoc />
        public void DisposeIsolatedInstance(Guid instanceId)
        {
            Contract.Requires(instanceId != Guid.Empty);

            IIsolatedExecutor instance;

            lock (this.executorsSync)
            {
                instance = this.executorsList.FirstOrDefault(e => e.InstanceId == instanceId);
            }

            if (instance == null)
            {
                throw new SandboxException("Sandbox does not contain instance with id=" + instanceId);
            }

            this.DisposeIsolatedInstance(instance);
        }

        /// <inheritdoc />
        public void DisposeIsolatedInstance(IIsolatedExecutor instance)
        {
            Contract.Requires(instance != null);

            lock (this.executorsSync)
            {
                if (!this.executorsList.Contains(instance))
                {
                    throw new SandboxException("Sandbox does not contain instance with id=" + instance.InstanceId);
                }

                this.executorsList.Remove(instance);
            }

            instance.Dispose();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose wrapped instance.
        /// </summary>
        /// <param name="disposing">Whether the call is coming from
        /// Dispose (as opposed to Finalize)</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.disposed)
                {
                    return;
                }

                lock (this.executorsSync)
                {
                    if (this.executorsList != null)
                    {
                        foreach (var executor in this.executorsList)
                        {
                            if (executor != null)
                            {
                                executor.Dispose();
                            }
                        }

                        this.executorsList.Clear();
                        this.executorsList = null;
                    }
                }

                if (this.IsolationDomain != null)
                {
                    AppDomain.Unload(this.IsolationDomain);
                    this.IsolationDomain = null;
                }

                this.disposed = true;
            }
        }
    }
}
