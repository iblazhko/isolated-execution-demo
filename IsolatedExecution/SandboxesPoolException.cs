﻿namespace IsolatedExecution
{
    using System;

    /// <summary>
    /// Represents sandboxpool-related exceptions.
    /// </summary>
    public class SandboxesPoolException: Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxesPoolException"/> class.
        /// </summary>
        public SandboxesPoolException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxesPoolException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public SandboxesPoolException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SandboxesPoolException"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">Underlying exception instance.</param>
        public SandboxesPoolException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
