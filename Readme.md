Isolated Execution
==================

Version 1.0
13/06/2015

Ivan Blazhko <ivan.blazhko@gmail.com>


Introduction
------------

Let's consider an application that need to be able to load a class instance
on demand in an isolated sandbox, and be able to unload the code from
memory when we are done with it.

In this article I will describe approach to loading and isolating execution
code at runtime using .NET AppDomain.


Solution Structure
------------------

###Introduction

There are following key entities:

  - Executor: this is the code we need to isolate
  - Sandbox: isolated environment that can contain Executors
  - SandboxesPool: main entity, provides functionality to create
    isolated Executor instance in a dedicated Sandbox or a shared Sandbox.

Conclusion
----------

