﻿namespace IsolatedExecution.Demo
{
    using System;

    /// <summary>
    /// Sample executor used by the main application.
    /// </summary>
    [Serializable]
    public class ExecutorB
    {
        /// <summary>
        /// Sample method used by the main application.
        /// </summary>
        public void DoMoreStuff()
        {
            Console.WriteLine("Some more stuff from ExecutorB");
        }
    }
}
