﻿namespace IsolatedExecution.Demo
{
    using System;

    /// <summary>
    /// Sample executor used by the main application.
    /// </summary>
    [Serializable]
    public class ExecutorA : IDisposable
    {
        /// <summary>
        /// Indicates whether this object instance is disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        public bool IsDisposed
        {
            get { return this.disposed; }
        }

        /// <summary>
        /// Sample method used by the main application.
        /// </summary>
        public void DoStuff()
        {
            Console.WriteLine("Some stuff from ExecutorA");
        }

        /// <summary>
        /// Dispose this object instance.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose this object instance.
        /// </summary>
        /// <param name="disposing">Indicates whether this method is called from Dispose (as opposed to Finalize).</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.disposed)
                {
                    return;
                }

                this.disposed = true;
            }
        }
    }
}
