﻿namespace IsolatedExecution.Demo
{
    using System;
    
    /// <summary>
    /// Application demonstrates usage of execution isolation classes.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Application entry point.
        /// </summary>
        /// <param name="args">Application command line arguments (not used).</param>
        public static void Main(string[] args)
        {
            Console.WriteLine("Isolated executors test:");

            using (var pool = new SandboxesPool())
            {
                // 1)
                // create isolated instance in a new sandbox (default behaviour)
                var isolatedInstanceWrapperA1 = pool.CreateIsolatedInstance(typeof(ExecutorA));
                var isolatedInstanceA1 = isolatedInstanceWrapperA1.Instance as ExecutorA;
                isolatedInstanceA1.DoStuff();
               
                // 2)
                // create isolated instance in an available free sandbox. (no free sandboxes are available, so a new one will be created)
                var isolatedInstanceWrapperA2 = pool.CreateIsolatedInstance(typeof(ExecutorA), ExecutorIsolationMode.FreeSandbox);
                var isolatedInstanceA2 = isolatedInstanceWrapperA2.Instance as ExecutorA;
                isolatedInstanceA2.DoStuff();

                // 3)
                // create isolated instance in a least utilized sandbox (which will be the one created in step 1))
                var isolatedInstanceWrapperA3 = pool.CreateIsolatedInstance(typeof(ExecutorA), ExecutorIsolationMode.LeastUtilizedSandbox);
                var isolatedInstanceA3 = isolatedInstanceWrapperA3.Instance as ExecutorA;
                isolatedInstanceA3.DoStuff();

                // 4)
                // create several isolated instances in a specific sandbox
                var sandboxId = pool.CreateExecutorSandbox();
                
                var isolatedInstanceWrapperA4 = pool.CreateIsolatedInstance(typeof(ExecutorA), sandboxId);
                var isolatedInstanceA4 = isolatedInstanceWrapperA4.Instance as ExecutorA;               
                isolatedInstanceA4.DoStuff();

                var isolatedInstanceWrapperB1 = pool.CreateIsolatedInstance(typeof(ExecutorB), sandboxId);
                var isolatedInstanceB1 = isolatedInstanceWrapperB1.Instance as ExecutorB;
                isolatedInstanceB1.DoMoreStuff();

                // 5)
                // create isolated instance in an available free sandbox
                pool.CreateExecutorSandbox();
                var isolatedInstanceWrapperA5 = pool.CreateIsolatedInstance(typeof(ExecutorA), ExecutorIsolationMode.FreeSandbox);
                var isolatedInstanceA5 = isolatedInstanceWrapperA5.Instance as ExecutorA;
                isolatedInstanceA5.DoStuff();
            }

            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
