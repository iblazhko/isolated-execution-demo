﻿namespace IsolatedExecution.Tests
{
    using System;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public sealed class IsolatedExecutorTests
    {
        [Test]
        public void Given_IsolatedExecutor_When_InstantiatedWithIdAndInstance_Then_IdAndInstancePropertiesAreInitialized()
        {
            // Arrange
            var instance = new object();
            var instanceId = Guid.NewGuid();
            var sandboxId = Guid.NewGuid();

            // Act
            var wrapper = new IsolatedExecutor(instance, sandboxId, instanceId);

            // Assert
            Assert.AreSame(instance, wrapper.Instance);
            Assert.AreEqual(instanceId, wrapper.InstanceId);
        }

        [Test]
        public void Given_IsolatedExecutor_When_InstantiatedWithoutExplicitId_Then_NewRandomIdIsAssigned()
        {
            // Arrange
            var sandboxId = Guid.NewGuid();

            // Act
            var wrapper1 = new IsolatedExecutor(new object(), sandboxId);
            var wrapper2 = new IsolatedExecutor(new object(), sandboxId);

            // Assert
            Assert.AreNotEqual(Guid.Empty, wrapper1.InstanceId);
            Assert.AreNotEqual(Guid.Empty, wrapper2.InstanceId);
            Assert.AreNotEqual(wrapper1.InstanceId, wrapper2.InstanceId);
        }

        [Test]
        public void Given_IsolatedExecutor_When_Disposed_Then_WrappedInstanceIsDisposed()
        {
            // Arrange
            var instance = new Mock<IDisposable>();
            instance.Setup(o => o.Dispose()).Verifiable();

            // Act
            var wrapper = new IsolatedExecutor(instance.Object, Guid.NewGuid());
            wrapper.Dispose();

            // Assert
            instance.VerifyAll();
        }
    }
}
