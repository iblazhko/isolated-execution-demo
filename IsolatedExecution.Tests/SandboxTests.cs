﻿namespace IsolatedExecution.Tests
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Proxies;
    using NUnit.Framework;

    [TestFixture]
    public sealed class SandboxTests
    {
        [Test]
        public void Given_Sandbox_When_InstantiatedWithoutExplicitId_Then_NewRandomIdIsAssigned()
        {
            // Arrange

            // Act
            ISandbox sandbox1 = new Sandbox();
            ISandbox sandbox2 = new Sandbox();

            // Assert
            Assert.AreNotEqual(Guid.Empty, sandbox1.Id);
            Assert.AreNotEqual(Guid.Empty, sandbox2.Id);
            Assert.AreNotEqual(sandbox1.Id, sandbox2.Id);
        }

        [Test]
        public void Given_Sandbox_When_Instantiated_Then_IsolationAppDomainIsCreatedWithTheNameThatMatchesSandboxId()
        {
            // Arrange

            // Act
            var sandbox = new Sandbox();

            // Assert
            Assert.IsNotNull(sandbox.IsolationDomain);
            Assert.IsTrue(sandbox.IsolationDomain.FriendlyName.EndsWith(sandbox.Id.ToString()));
        }

        [Test]
        public void Given_Sandbox_When_Instantiated_Then_ExecutorsListIsEmpty()
        {
            // Arrange

            // Act
            ISandbox sandbox = new Sandbox();

            // Assert
            Assert.IsNotNull(sandbox.Executors);
            Assert.AreEqual(0, sandbox.Executors.Count());
        }

        [Test]
        public void Given_Sandbox_When_CreateIsolatedInstanceIsCalled_Then_InstanceIsCreatedInIsolationDomain()
        {
            // Arrange
            ISandbox sandbox = new Sandbox();

            // Act
            var wrapper = sandbox.CreateIsolatedInstance(typeof(AnExecutor));

            // Assert
            // TODO: check AppDomain of the wrapper
        }

        [Test]
        public void Given_SandboxWithExecutorAndExclusiveSharingMode_When_CreateIsolatedInstanceIsCalled_Then_ExceptionIsThrown()
        {
            // Arrange
            ISandbox sandbox = new Sandbox(SandboxSharingMode.Exclusive);
            var wrapper1 = sandbox.CreateIsolatedInstance(typeof(AnExecutor));
            SandboxException ex = null;

            // Act
            try
            {
                var wrapper2 = sandbox.CreateIsolatedInstance(typeof(AnExecutor));
            }
            catch (SandboxException e)
            {
                ex = e;
            }

            // Assert
            Assert.IsNotNull(ex);
            Assert.IsTrue(ex.Message.StartsWith("Dedicated sandbox cannot contain multiple isolated instances"));
        }

        [Test]
        public void Given_Sandbox_When_CreateIsolatedInstanceIsCalled_Then_InstanceIsAddedToExecutorsCollection()
        {
            // Arrange
            ISandbox sandbox = new Sandbox();

            // Act
            var wrapper = sandbox.CreateIsolatedInstance(typeof(AnExecutor));

            // Assert
            Assert.AreEqual(1, sandbox.Executors.Count());
            Assert.AreEqual(wrapper.InstanceId, sandbox.Executors.First().InstanceId);
        }

        [Test]
        public void Given_Sandbox_When_DisposeIsolatedInstanceIsCalled_Then_InstanceIsDisposedAndRemovedFromExecutorsCollection()
        {
            // Arrange
            ISandbox sandbox = new Sandbox();
            var wrapper = sandbox.CreateIsolatedInstance(typeof(AnExecutor));
            var wrappedInstance = wrapper.Instance as AnExecutor;
            Assert.IsNotNull(wrappedInstance);
            Assert.IsFalse(wrappedInstance.IsDisposed);

            // Act
            sandbox.DisposeIsolatedInstance(wrapper);

            // Assert
            Assert.AreEqual(0, sandbox.Executors.Count());
            Assert.IsTrue(wrappedInstance.IsDisposed);
        }

        [Test]
        public void Given_Sandbox_When_DisposeIsolatedInstanceByIdIsCalled_Then_InstanceIsDisposedAndRemovedFromExecutorsCollection()
        {
            // Arrange
            ISandbox sandbox = new Sandbox();
            var wrapper = sandbox.CreateIsolatedInstance(typeof(AnExecutor));
            var wrappedInstance = wrapper.Instance as AnExecutor;
            Assert.IsNotNull(wrappedInstance);
            Assert.IsFalse(wrappedInstance.IsDisposed);

            // Act
            sandbox.DisposeIsolatedInstance(wrapper.InstanceId);

            // Assert
            Assert.AreEqual(0, sandbox.Executors.Count());
            Assert.IsTrue(wrappedInstance.IsDisposed);
        }

        [Test]
        public void Given_Sandbox_When_Disposed_Then_AllExecutorsAreDisposedAndExecutorsListIsCleared()
        {
            // Arrange
            ISandbox sandbox = new Sandbox();

            // Act
            sandbox.Dispose();

            // Assert
            Assert.AreEqual(0, sandbox.Executors.Count());
        }

        [Test]
        public void Given_Sandbox_When_Disposed_Then_IsolationAppDomainIsUnloaded()
        {
            // Arrange
            AppDomain isolationDomain = null;
            var sandbox = new Sandbox(
                Guid.NewGuid(),
                (name, setupFactory) =>
                    {
                        isolationDomain = Sandbox.CreateIsolationDomain(name, setupFactory);
                        return isolationDomain;
                    },
                () =>
                    {
                        return Sandbox.GetIsolationDomainSetup();
                    });
            Assert.AreSame(isolationDomain, sandbox.IsolationDomain);

            // Act
            sandbox.Dispose();

            // Assert
            Assert.IsNull(sandbox.IsolationDomain);

            var isUnloaded = false;
            try
            {
                // operation on an unloaded app domain should cause AppDomainUnloadedException
                var b = isolationDomain.BaseDirectory;
                isUnloaded = false;
            }
            catch (AppDomainUnloadedException)
            {
                isUnloaded = true;
            }

            Assert.IsTrue(isUnloaded);
        }
    }
}
