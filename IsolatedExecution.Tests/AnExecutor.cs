﻿namespace IsolatedExecution.Tests
{
    using System;

    /// <summary>
    /// Sample executor implementation used by te unit tests.
    /// </summary>
    [Serializable]
    public class AnExecutor : IDisposable
    {
        private bool disposed;

        /// <summary>
        /// Indicates whether the instance is disposed.
        /// </summary>
        public bool IsDisposed
        {
            get { return this.disposed; }
        }

        /// <summary>
        /// Sample method called by the consumers.
        /// </summary>
        public void DoStuff()
        {
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose this instance.
        /// </summary>
        /// <param name="disposing">Indicates whether the call is coming from Dispose (as opposed to Finalize).</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.disposed)
                {
                    return;
                }

                this.disposed = true;
            }
        }
    }
}
