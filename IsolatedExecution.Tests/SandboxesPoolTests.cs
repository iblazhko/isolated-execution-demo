﻿namespace IsolatedExecution.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    public sealed class SandboxesPoolTests
    {
        [Test]
        public void Given_SandboxesPool_When_Instantiated_Then_SandboxesListIsEmpty()
        {
            // Arrange

            // Act
            ISandboxesPool pool = new SandboxesPool();

            // Assert
            Assert.IsNotNull(pool.Sandboxes);
            Assert.AreEqual(0, pool.Sandboxes.Count());
        }

        [Test]
        public void Given_SandboxesPool_When_CreateExecutorSandboxIsCalled_Then_NewSandboxIsAddedToList()
        {
            // Arrange
            ISandboxesPool pool = new SandboxesPool();

            // Act
            var sandboxId = pool.CreateExecutorSandbox();

            // Assert
            Assert.AreNotEqual(Guid.Empty, sandboxId);
            Assert.AreEqual(1, pool.Sandboxes.Count());
            Assert.AreEqual(sandboxId, pool.Sandboxes.First().Id);
        }

        [Test]
        public void Given_SandboxesPool_When_DisposeExecutorSandboxIsCalled_Then_SandboxIsRemovedFromList()
        {
            // Arrange
            ISandboxesPool pool = new SandboxesPool();
            var sandboxId = pool.CreateExecutorSandbox();
            Assert.AreEqual(1, pool.Sandboxes.Count());

            // Act
            pool.DisposeExecutorSandbox(sandboxId);

            // Assert
            Assert.AreEqual(0, pool.Sandboxes.Count());
        }

        [Test]
        public void Given_SandboxesPool_When_CreateIsolatedInstanceIsCalled_Then_InstanceIsAddedToNewSandbox()
        {
            // Arrange
            ISandboxesPool pool = new SandboxesPool();

            // Act
            var wrapper = pool.CreateIsolatedInstance(typeof(AnExecutor));

            // Assert
            Assert.AreEqual(1, pool.Sandboxes.Count());
            Assert.AreEqual(1, pool.Sandboxes.First().Executors.Count());
            Assert.AreEqual(wrapper.InstanceId, pool.Sandboxes.First().Executors.First().InstanceId);
        }

        [Test]
        public void Given_SandboxesPool_When_CreateIsolatedInstanceIsCalledWithSandboxId_Then_InstanceIsAddedToSpecified()
        {
            // Arrange
            ISandboxesPool pool = new SandboxesPool();
            var sandbox1Id = pool.CreateExecutorSandbox();
            var sandbox2Id = pool.CreateExecutorSandbox();

            // Act
            var wrapper = pool.CreateIsolatedInstance(typeof(AnExecutor), sandbox1Id);

            // Assert
            Assert.AreEqual(2, pool.Sandboxes.Count());
            Assert.AreEqual(1, pool.Sandboxes.Where(s => s.Id == sandbox1Id).First().Executors.Count());
            Assert.AreEqual(0, pool.Sandboxes.Where(s => s.Id == sandbox2Id).First().Executors.Count());
            Assert.AreEqual(wrapper.InstanceId, pool.Sandboxes.Where(s => s.Id == sandbox1Id).First().Executors.First().InstanceId);
        }

        [Test]
        public void Given_SandboxesPool_When_DisposeIsolatedInstanceIsCalled_Then_InstanceIsDisposedAndRemovedFromExecutorsCollection()
        {
            // Arrange
            ISandboxesPool pool = new SandboxesPool();
            var wrapper = pool.CreateIsolatedInstance(typeof(AnExecutor));
            var wrappedInstance = wrapper.Instance as AnExecutor;
            Assert.IsNotNull(wrappedInstance);
            Assert.IsFalse(wrappedInstance.IsDisposed);

            // Act
            pool.DisposeIsolatedInstance(wrapper.InstanceId);

            // Assert
            Assert.IsTrue(wrappedInstance.IsDisposed);
        }

        [Test]
        public void Given_SandboxesPool_When_DisposeIsolatedInstanceIsCalledWithAutoSandboxDisposalOption_Then_CorrespondingSandboxIsDisposed()
        {
            // Arrange
            ISandboxesPool pool = new SandboxesPool();
            var sandbox1Id = pool.CreateExecutorSandbox();
            var sandbox2Id = pool.CreateExecutorSandbox();

            var wrapper = pool.CreateIsolatedInstance(typeof(AnExecutor), sandbox1Id);
            var wrappedInstance = wrapper.Instance as AnExecutor;
            Assert.IsNotNull(wrappedInstance);
            Assert.IsFalse(wrappedInstance.IsDisposed);

            // Act
            pool.DisposeIsolatedInstance(wrapper.InstanceId, SandboxAutoDisposalMode.DisposeIfFree);

            // Assert
            Assert.IsTrue(wrappedInstance.IsDisposed);
            Assert.AreEqual(1, pool.Sandboxes.Count());
            Assert.AreEqual(0, pool.Sandboxes.Where(s => s.Id == sandbox1Id).Count());
        }

        [Test]
        public void Given_SandboxesPool_When_DisposeIsolatedInstanceIsCalledWithAutoSandboxDisposalOptionAndSandboxHasMoreExecutors_Then_CorrespondingSandboxIsNotDisposed()
        {
            // Arrange
            ISandboxesPool pool = new SandboxesPool();
            var sandbox1Id = pool.CreateExecutorSandbox();
            var sandbox2Id = pool.CreateExecutorSandbox();

            var wrapper1 = pool.CreateIsolatedInstance(typeof(AnExecutor), sandbox1Id);
            var wrapped1Instance = wrapper1.Instance as AnExecutor;
            Assert.IsNotNull(wrapped1Instance);
            Assert.IsFalse(wrapped1Instance.IsDisposed);

            var wrapper2 = pool.CreateIsolatedInstance(typeof(AnExecutor), sandbox1Id);
            var wrapped2Instance = wrapper2.Instance as AnExecutor;
            Assert.IsNotNull(wrapped2Instance);
            Assert.IsFalse(wrapped2Instance.IsDisposed);

            // Act
            pool.DisposeIsolatedInstance(wrapper1.InstanceId, SandboxAutoDisposalMode.DisposeIfFree);

            // Assert
            Assert.IsTrue(wrapped1Instance.IsDisposed);
            Assert.IsFalse(wrapped2Instance.IsDisposed);
            Assert.AreEqual(2, pool.Sandboxes.Count());
            Assert.AreEqual(1, pool.Sandboxes.Where(s => s.Id == sandbox1Id).Count());
            Assert.AreEqual(wrapper2.InstanceId, pool.Sandboxes.Where(s => s.Id == sandbox1Id).First().Executors.First().InstanceId);
        }

        // TODO: test various ExecutorIsolationMode options
    }
}
